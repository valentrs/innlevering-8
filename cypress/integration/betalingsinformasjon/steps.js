import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, function () {
    cy.visit('http://localhost:8080');

    cy.get('#emptyCart').click();

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();
})

And(/^trykket på Gå til betaling$/, function () {
    cy.get('#goToPayment').click(); 
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, function () {
    cy.get('#fullName').type('Vali ST');
    cy.get('#address').type('Noeveien 2');
    cy.get('#postCode').type('1234');
    cy.get('#city').type('Oslo');
    cy.get('#creditCardNo').type('1234542132146782');
});

And(/^trykker på Fullfør kjøp$/, function () {
    cy.get('form').submit();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, function () {
    cy.contains('Din ordre er registrert.').should('exist');
});

When(/^jeg legger inn ugyldige verdier i feltene$/, function () {
    cy.get('#fullName').focus().blur();
    cy.get('#address').focus().blur(); 
    cy.get('#postCode').focus().blur(); 
    cy.get('#city').focus().blur(); 
    cy.get('#creditCardNo').clear().type('123').blur(); 
});

Then(/^skal jeg få feilmeldinger for disse$/, function () {
    cy.get('#fullNameError').should('contain', 'Feltet må ha en verdi');
    cy.get('#addressError').should('contain', 'Feltet må ha en verdi');
    cy.get('#postCodeError').should('contain', 'Feltet må ha en verdi');
    cy.get('#cityError').should('contain', 'Feltet må ha en verdi');
    cy.get('#creditCardNoError').should('contain', 'Kredittkortnummeret må bestå av 16 siffer');
})