import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#list').within(() => {
        cy.contains('Hubba bubba').should('exist');
        cy.contains('Smørbukk').should('exist');
        cy.contains('Stratos').should('exist');
        cy.contains('Hobby').should('exist');
    });
});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '33');
});


And(/^lagt inn varer og kvanta$/, function () {
    cy.get('#emptyCart').click();
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

When(/^jeg sletter varer$/, function () {
    
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#deleteItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#deleteItem').click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet$/, function () {
    cy.get('#price').should('have.text', '20');
});

When(/^jeg oppdaterer kvanta for en vare$/, function () {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('3');
    cy.get('#saveItem').click();
})
 
Then(/^skal handlekurven inneholde riktig kvanta for varen$/, function () {
    cy.get('#price').should('have.text', '31');
})